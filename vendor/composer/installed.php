<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'a7b99dffa7a8fd9c8c98c86b0d4dd58d07c91c6c',
        'name' => 'sevencommonfactor/payunit',
        'dev' => true,
    ),
    'versions' => array(
        'sevencommonfactor/payunit' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'a7b99dffa7a8fd9c8c98c86b0d4dd58d07c91c6c',
            'dev_requirement' => false,
        ),
    ),
);
